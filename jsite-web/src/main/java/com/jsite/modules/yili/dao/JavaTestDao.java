/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.yili.dao;

import com.jsite.common.persistence.CrudDao;
import com.jsite.common.persistence.annotation.MyBatisDao;
import com.jsite.modules.yili.entity.JavaTest;
import org.apache.ibatis.annotations.Param;

/**
 * JAVA机试表生成DAO接口
 * @author liuruijun
 * @version 2020-06-09
 */
@MyBatisDao
public interface JavaTestDao extends CrudDao<JavaTest> {

    JavaTest getByName(@Param("name") String name, @Param("officeID") String officeID);

    int getCount();
}