#! /bin/sh
# 1. kill,防止改端口   
sh shutdown.sh 8081
# 2. pull
git pull origin master
# 3. install
mvn clean install
# 4. run
cd jsite-web
pwd
nohup mvn clean spring-boot:run &
tail -F nohup.out 
