package com.jsite.modules.flowable.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jsite.common.persistence.DataEntity;
import com.jsite.common.utils.excel.annotation.ExcelField;
import com.jsite.modules.sys.entity.User;

import java.util.Date;

public class RuExecution extends DataEntity<RuExecution> {

	private static final long serialVersionUID = 1L;

	protected String name;
	protected String taskDefinitionKey; // 任务定义Key（任务环节标识）
	protected String taskName;
	protected String processInstanceId;
	protected String processDefinitionId;
	protected String activityId;
	protected int suspensionState;
	protected String executionId;

	protected Date startTime;
	protected User startUser;

    private String status; 		// 任务状态（todo/claim/finish）待办、待签收、已办已发

	@ExcelField(title="流程名称", align=2, sort=10)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTaskDefinitionKey() {
		return taskDefinitionKey;
	}

	public void setTaskDefinitionKey(String taskDefinitionKey) {
		this.taskDefinitionKey = taskDefinitionKey;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public int getSuspensionState() {
		return suspensionState;
	}

	public void setSuspensionState(int suspensionState) {
		this.suspensionState = suspensionState;
	}

	public String getExecutionId() {
		return executionId;
	}

	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}

	@ExcelField(title="流程发起人", align=2, sort=20)
    public User getStartUser() {
        return startUser;
    }

    public void setStartUser(User startUser) {
        this.startUser = startUser;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="发起时间", align=2, sort=30)
    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
